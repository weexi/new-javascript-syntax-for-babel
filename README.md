# new-javascript-syntax-for-babel

#### 介绍
未来JavaScript语法新特性测试

#### 软件架构
1.本项目用于测试JavaScript最新语法新特性。<br>
2.所有测试技术由Babel插件支持。<br>
3.截止目前2019/10，支持所有ECMAScript新语法测试。

#### 使用说明

1.  git clone https://gitee.com/weexi/new-javascript-syntax-for-babel.git
2.  npm i/cnpm i
3.  npm run start

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
