const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.config')
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
const portfinder = require('portfinder')
const devConfig = merge(baseWebpackConfig, {
	devtool: 'source-map',
	devServer: {
		clientLogLevel: 'warning',
		hot: true,
		contentBase: false,
		compress: true,
		host: process.env.HOST,
		port: 3000,
		open: true,
		overlay: {
			warnings: false,
			errors: true
		},
		publicPath: '/',
		proxy: {},
		quiet: true,
		watchOptions: {
			poll: true
		}
	}
})

module.exports = new Promise((resolve, reject) => {
	portfinder.basePort = 3000
	portfinder.getPort((err, port) => {
		if (err) {
			reject(err)
		} else {
			process.env.PORT = port
			devConfig.devServer.port = port
			devConfig.plugins.push(new FriendlyErrorsPlugin({
				compilationSuccessInfo: {
					messages: [`你的项目运行在此: http://location:${port}`],
				}
			}))
			resolve(devConfig)
		}
	})
})
